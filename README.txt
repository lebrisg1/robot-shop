#Project cloned from https://github.com/instana/robot-shop

oc new-project robot-shop
oc adm policy add-scc-to-user anyuid -z default -n robot-shop
oc adm policy add-scc-to-user privileged -z default -n robot-shop

oc apply -f k8s -n robot-shop
oc apply -f istio -n robot-shop

oc apply -f load-deployment.yaml -n robot-shop
